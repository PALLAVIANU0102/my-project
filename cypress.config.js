const { defineConfig } = require('cypress')
const cucumber= require('cypress-cucumber-preprocessor').default;

module.exports = defineConfig({
  video:false,
  viewportWidth:1920,
  viewportHeight:1080,
  pageLoadTimeout:90000,
  requestTimeout:30000,
  responseTimeout:30000,
  defaultCommandTimeout:10000,
  execTimeout:60000,
  animationDistanceThreshold:3,
  screenshotOnRunFailure:false,
  screenshotsFolder:'reports/schreenshots',
  downloadsFolder:'cypress/dowloads',
  trashAssetsBeforeRuns:true,
  watchForFileChanges:true,
  chromeWebSecurity:false,
  waitForAnimations:true,
  failOnStatusCode:false,
  blockHost:['*wootric.com', '*.herokuapp.com'],
  retries: {
    runMode:1,
  },
  env:
  {
    TAGS:"not (@ignore or @ignoreInDev)",
    evvironment: "qa",
    snackbarWaitTime:2500
  },
  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
      on('file:preprocessor', cucumber());
    },
    baseUrl: 'https://qa-bcom.coldwellbanker.com/',
    specPattern:'cypress/e2e/**/*.feature',
    supportFile:'cypress/support/e2e.js',
    slowTestThreshold:30000,
    excludeSpecPattern:['*.js', '*.md']
  },
})